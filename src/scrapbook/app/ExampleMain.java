package scrapbook.app;

import scrapbook.ExampleAPI;
import scrapbook.impl.VersionHandler;

public class ExampleMain {

	public static void main(String[] args) {
		VersionHandler.setVersion(VersionHandler.V2);
		String result = ExampleAPI.OBJECT.evenBetter();
		System.out.println(result);
		
		VersionHandler.setVersion(VersionHandler.V1);
		result = ExampleAPI.OBJECT.evenBetter();
		System.out.println(result);
	}
}
