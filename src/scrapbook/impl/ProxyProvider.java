package scrapbook.impl;

import java.lang.reflect.Proxy;

import scrapbook.IObject;

public class ProxyProvider {

	public static IObject getIObjectAPI() {
		return (IObject) Proxy.newProxyInstance(IObject.class.getClassLoader(), new Class[] { IObject.class }, new ObjectHandler());
	}
}
