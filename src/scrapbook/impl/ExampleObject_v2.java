package scrapbook.impl;

import scrapbook.IObject;

public class ExampleObject_v2 implements IObject {

	@Override
	public String veryInterestingBehaviour() {
		return "veryInterestingBehaviour comming from v2";
	}

	@Override
	public String evenBetter() {
		return "Wow, isnt that a great method to be calling right now. ROFL, MOFL, BLA BLA!";
	}
}
