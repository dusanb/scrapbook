package scrapbook.impl;

import scrapbook.IObject;

public class ExampleObject_v1 implements IObject {

	@Override
	public String veryInterestingBehaviour() {
		return "veryInterestingBehaviour comming from v1";
	}

	@Override
	public String evenBetter() {
		throw new UnsupportedOperationException("It actualy should never come to this!");
	}
}
