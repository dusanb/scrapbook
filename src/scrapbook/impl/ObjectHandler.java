package scrapbook.impl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import scrapbook.IObject;

public class ObjectHandler implements InvocationHandler {

	private IObject object;

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		String version = VersionHandler.getVersion();
		this.object = ObjectProvider.getThaMuthafucka();
		if (IObject.Methods.EVEN_BETTER.equals(method.getName())) {
			if (VersionHandler.V1.equals(version)) {
				return this.object.veryInterestingBehaviour();
			}
			return this.object.evenBetter();
		}
		return null;
	}
}
