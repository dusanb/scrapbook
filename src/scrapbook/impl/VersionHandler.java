package scrapbook.impl;

public class VersionHandler {

	public static final String KEY = "tha.muthafucka.version";

	public static final String V1 = "v1";
	public static final String V2 = "v2";

	public static void setVersion(String version) {
		System.setProperty(KEY, version);
	}

	public static String getVersion() {
		return System.getProperty(KEY, V1);
	}
}
