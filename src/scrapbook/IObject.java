package scrapbook;

public interface IObject {

	static interface Methods {
		static final String VERY_INTERESTING_BEHAVIOUR = "veryInterestingBehaviour";
		static final String EVEN_BETTER = "evenBetter";
	}

	String veryInterestingBehaviour();

	String evenBetter();
}
